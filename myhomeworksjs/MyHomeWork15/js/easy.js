let min = 0, sec = 0, msc = 0, timerID = 0;
let mscSpan = document.getElementsByClassName('miliseconds')[0];
let secSpan = document.getElementsByClassName('seconds')[0];
let minSpan = document.getElementsByClassName('minutes')[0];
let startBtn = document.getElementById('start');
let pauseBtn = document.getElementById('pause');
let stopBtn = document.getElementById('clear');
startBtn.addEventListener('click', startTimer);
stopBtn.addEventListener('click', clear);
pauseBtn.addEventListener('click', pause);

function startTimer() {
  timerID = setInterval(() => {
    ++msc;
    if (msc === 100) {
      msc = 0;
      ++sec;
    }
    if (sec === 60) {
      sec = 0;
      ++min;
    }
    updateTimer()
  }, 10)
  showPauseBtn();
}
function pause() {
  clearInterval(timerID);
  showStartBtn()
}
function clear() {
  clearInterval(timerID);
  min = 0, sec = 0, msc = 0;
  mscSpan.innerText = '00';
  secSpan.innerText = '00';
  minSpan.innerText = '00';
  showStartBtn()
}
function showStartBtn() {
  startBtn.style.display = 'inline-block';
  pauseBtn.style.display = 'none';
}
function showPauseBtn() {
  startBtn.style.display = 'none';
  pauseBtn.style.display = 'inline-block';
}
function updateTimer() {
  mscSpan.innerText = addZero(msc);
  secSpan.innerText = addZero(sec);
  minSpan.innerText = addZero(min);
}
function addZero(time) {
  if (time <= 9) {
    return `0${time}`;
  }
  return `${time}`;
}