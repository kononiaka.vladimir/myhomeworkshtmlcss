let pushBtn = document.getElementsByClassName('btn')[0]
    .addEventListener('click', getInput);

function getInput() {
    let inputDiameter = document.createElement('input');
    let btnCreate = document.createElement('button');
    inputDiameter.id = 'diameter';
    inputDiameter.placeholder = 'Enter diameter in px';
    document.body.appendChild(inputDiameter);
    btnCreate.id = 'create';
    btnCreate.innerText = 'Нарисовать круг'
    document.body.appendChild(btnCreate);
    let btn = document.getElementsByClassName('btn')[0];
    btn.remove();

    btnCreate.addEventListener('click', createCircles);

}

function createCircles() {
    let diameter = document.getElementById('diameter');
    let body = document.getElementsByTagName('body');
    document.getElementById('create').remove();
    document.getElementById('diameter').remove();

    for (let i = 1; i <= 100; i++) {
        if(i % 10 === 1 ) {
        document.body.appendChild(document.createElement('br'));
        }

        let circle = document.createElement('div');
        circle.classList.add('circle');
        circle.style.width = circle.style.height = diameter.value + 'px';
        circle.style.borderRadius = '50%';
        circle.style.display = 'inline-block';
        circle.style.marginRight = '5px';
        circle.style.backgroundColor = randomColor();
        document.body.appendChild(circle);
    }
    document.querySelectorAll('div').forEach(function (e) {
        e.onclick = () => e.style.visibility = 'hidden';
    })
}
function randomColor() {
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += 'ABCDEF01234567890'.charAt(Math.round(Math.random() * 15))
    }
    return color;
}
