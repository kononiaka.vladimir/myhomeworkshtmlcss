let btn = document.getElementsByClassName('btn')[0];
btn.addEventListener('click', function (event) {
    btn.remove();
    let inputDiameter = document.createElement('input');
    let inputColor = document.createElement('input');
    let btnDraw = document.createElement('button');

    inputDiameter.classList.add('diameter');
    inputColor.classList.add('color');

    let body = document.getElementsByTagName('body')[0];

    body.appendChild(inputDiameter);
    inputDiameter.style.margin = '15px';
    inputDiameter.placeholder = 'Enter diameter';

    body.appendChild(inputColor);
    inputColor.placeholder = 'Enter color';

    body.appendChild(btnDraw);
    btnDraw.textContent = 'Create';
    btnDraw.style.marginLeft = '20px';

    btnDraw.addEventListener('click', function (e) {

        let circle = document.createElement('div');

        if (document.querySelector('div')) {
            document.getElementsByTagName('div')[0].remove();

            inputDiameter.placeholder = 'Enter diameter';
            inputColor.placeholder = 'Enter color';
        }
        let diameterValue = inputDiameter.value;
        let colorValue = inputColor.value;

        circle.style.width = diameterValue + 'px';
        circle.style.height = diameterValue + 'px';
        circle.style.backgroundColor = colorValue;
        circle.style.borderRadius = '50%';
        circle.style.marginTop = '15px'
        body.appendChild(circle);
    })
})