function Burger(size, stuffing) {
    this.size = size;
    this.stuffing = stuffing;
    this.topping = [];
}
Burger.SIZE_SMALL = {
    price: 50,
    cal: 20,
}
Burger.SIZE_LARGE = {
    price: 100,
    cal: 40,
}