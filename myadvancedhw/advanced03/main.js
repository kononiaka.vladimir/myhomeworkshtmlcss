class Burger {
    constructor(size, ...args) {
        try {
            if (!size) throw new CookErr(`You don't put correct size`);
            if (size.type !== 'size') throw new CookErr('Invalid type of size');
            if (!args.length) throw new CookErr(`You don't put any stuffing`);

            args.forEach(function (elem) {
                if (!elem) throw new CookErr(`You don't put any stuffing`);
                if (elem.type !== 'stuffing') throw new CookErr('Invalid type of stuffing');
            })
            this.size = size;
            this.stuffing = args;
            this.topping = [];

        }
        catch (err) {
            console.log(err);
        }
    }
    get sizeBurger() {
        return this.size;
    }
    get stuffingBurger() {
        return this.stuffing;
    }
    calcPrice = function () {
        let price = this.size.price;

        this.stuffing.forEach(function (item) {
            price += item.price;

        })
        this.topping.forEach(function (item) {
            price += item.price;
        })
        return price;
    }
    calcCal = function () {
        let cal = this.size.cal;

        this.stuffing.forEach(function (item) {
            cal += item.cal;
        })
        this.topping.forEach(function (item) {
            cal += item.cal;
        })
        return cal;
    }
    removeTopping = function (topping) {
        try {
            if (topping.type !== 'topping') {
                throw new CookErr('Please, put correct topping');
            }
            this.topping = this.topping.filter(arg => {
                if (arg.name !== topping.name) return arg;
            })

            return this.topping
        }
        catch (err) {
            console.error(err);
        }
    }
    get toppingBurger() {
        return this.topping;
    }
    set toppingBurger(topping) {
        try {
            if (!topping) {
                throw new CookErr(`You don't put wrong topping`);
            }
            if (topping.type !== 'topping') throw new CookErr('Please, put correct topping');
            this.topping.push(topping);
        }
        catch (err) {
            console.error(err);
        }
    };

    static get SIZE_SMALL() {
        return { price: 50, cal: 20, type: 'size' }
    }
    static get SIZE_LARGE() {
        return { price: 100, cal: 40, type: 'size' }
    }
    static get STUFFING_CHEESE() {
        return { price: 10, cal: 20, type: 'stuffing' }
    }
    static get STUFFING_SALAD() {
        return { price: 20, cal: 5, type: 'stuffing' }
    }
    static get STUFFING_POTATO() {
        return { price: 15, cal: 10, type: 'stuffing' }
    }
    static get TOPPING_SPICE() {
        return { price: 15, cal: 0, type: 'topping', name: "spice" }
    }
    static get TOPPING_MAYO() {
        return { price: 20, cal: 5, type: 'topping', name: "mayo" }
    }
}

class CookErr extends Error {
    constructor(message) {
        super();
        this.message = 'Cook Denied. ' + message;
    }
}

let burger = new Burger(Burger.SIZE_SMALL, Burger.STUFFING_CHEESE);
let burger2 = new Burger(Burger.SIZE_SMALL, Burger.STUFFING_CHEESE);
burger2.toppingBurger = Burger.TOPPING_MAYO
